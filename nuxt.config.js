const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'We\'re recruiting in Edinburgh | Edinburgh',
    meta: [
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { charset: 'utf-8' },
      { name: 'theme-color', content: '#2A2F42' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Little City Treat is a new way to book Beauty Appointments in Edinburgh. We are currently looking for qualified beauty therapists to find a new way of working.' },
      { hid: 'og:title', property: 'og:title', content: 'Little City Treat | Edinburgh' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:description', property: 'og:description', content: 'Little City Treat is a new way to book Beauty Appointments in Edinburgh. We are currently looking for qualified beauty therapists to find a new way of working.' },
      { hid: 'og:image', property: 'og:image', content: 'https://recruitment.littlecitytreat.com/little-city-treat.jpg' },
      { hid: 'og:image:width', property: 'og:image:width', content: '1200' },
      { hid: 'og:image:height', property: 'og:image:height', content: '627' },
      { hid: 'og:url', property: 'og:url', content: 'https://recruitment.littlecitytreat.com' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'canonical', href: 'https://recruitment.littlecitytreat.com' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display&display=swap' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#CB665E' }, // Fuzzy Wuzzy

  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/style.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/scroll.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/google-gtag'
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  **  Google Tag Manager Configuration
  */
  'google-gtag': {
    id: 'UA-145576340-4',
    config: {
      anonymize_ip: true, // anonymize IP
    },
    debug: true, // enable to track in dev mode
  },

  /*
  ** Build configuration
  */
  generate: {
  minify: {
      collapseWhitespace: false
    }
  },
  build: {
    /*
    ** You can extend webpack config here
    */
    babel: {
      presets: [
        ["@babel/preset-env", {
          "useBuiltIns": false,
        }],
      ],
      plugins: [
        "@babel/plugin-transform-runtime",
        "@babel/plugin-syntax-dynamic-import"
      ]
    },
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
