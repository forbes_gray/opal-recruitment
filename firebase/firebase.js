import firebase from 'firebase/app'
require('firebase/firestore')

const firebaseConfig = {
  apiKey: "AIzaSyD3KWKHZP-QHxqQFuCg_BUgoUl4TW9NWD4",
  databaseURL: "https://little-city-treat.firebaseio.com",
  projectId: "little-city-treat"
}

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : null

const db = firebase.firestore()
db.settings({ timestampsInSnapshots: true })

export default db
