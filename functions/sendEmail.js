const nodemailer = require('nodemailer')
const validate = require('validate.js')
import { smtpConfig } from '../config/env'

exports.handler = function(event, context, callback) {
    // Reject non-POST requests
    if (event.httpMethod !== 'POST') {
      console.log('This route only accepts POST requests');
      return callback(null, {
        statusCode: 400,
        body: "Only POST request accepted"
      })
    }

    /**
    * Validate form response
    */
    const data = JSON.parse(event.body)
    const { name, email, phone_number, skills, description } = data
    function validateEmail () {
      const constraints = {
        from: {
          email: true
        }
      }
      return email.length && !validate({ from: email }, constraints)
    }
    function validateNumber () {
      let number = phone_number.replace(/\D/g,'').replace(/^[ \t]+|[ \t]+$/, '')
      return number.length > 10 && number.length < 15
    }

    if (!(name.length && validate.isString(name))) {
      return callback(null, {
        statusCode: 422,
        body: 'Invalid Data type given for Name'
      })
    }
    if (!validateEmail()) {
      return callback(null, {
        statusCode: 422,
        body: 'Invalid Data type given for Email.'
      })
    }
    if (!validateNumber()) {
      return callback(null, {
        statusCode: 422,
        body: 'Invalid Data type given for Number.'
      })
    }
    if (!(description.length && validate.isString(description))) {
      return callback(null, {
        statusCode: 422,
        body: 'Invalid Data type given for Description'
      })
    }


    const receivers = ['forbes@littlecitytreat.com', 'debbie@littlecitytreat.com']

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: smtpConfig.email, // generated ethereal user
            pass: smtpConfig.password // generated ethereal password
        }
    })

    let mailOptions = {
        from: '"Recruitment Page" <' + smtpConfig.email + '>', // sender address
        to: receivers, // list of receivers
        subject: 'New Recruitment Submission', // Subject line
        text: `
          You have recieved a new submission from your recruitment page, \n
          Name: ${name}\n
          Email: ${email}\n
          Phone Number: ${phone_number}\n
          ${description}
        `, // plain text body
        html: `
          <div>
            <h4>You have recieved a new submission from your recruitment page</h4>
            <p>Name: ${name}</p><p>Email: ${email}</p><p>Phone Number: ${phone_number}</p><p>${description}</p>
          </div>
        `
    }

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
          return callback(null, {
            statusCode: 500,
            body: 'Unable to send Email'
          })
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        callback(null, {
          statusCode: 200,
          body: event.body
        })
    });

}
